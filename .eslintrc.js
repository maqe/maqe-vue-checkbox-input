module.exports = {
	root: true,
	env: {
		node: true,
		browser: true,
		jest: true
	},
	extends: [
		"maqe",
		"plugin:vue/essential"
	],
	rules: {
		"no-console": process.env.NODE_ENV === "production" ? "error" : "warn",
		"no-debugger": process.env.NODE_ENV === "production" ? "error" : "warn",
		"import/prefer-default-export": "off",
		"vue/html-indent": ["error", "tab"]
	},
	plugins: [
		"vue"
	],
	parserOptions: {
		parser: "babel-eslint"
	}
};
