# @maqe-vue/checkbox-input
The Vue2 component for checkbox-input

![label-insde](https://i.imgur.com/VvFaGDM.png)

See demo on: [Storybook](https://maqe-vue.qa.maqe.com/?path=/story/checkbox--all)

------

## Installation

#### NPM
Install the npm package.

```
npm install @maqe-vue/checkbox-input --save
```

#### Register the component

```if you want me to contribute, let me know na.
import VmqCheckbox from '@maqe-vue/checkbox-input'
import '@maqe-vue/checkbox-input/dist/style.css'

Vue.component('vmq-checkbox', VmqCheckbox)
```

------

## Usage

#### Basic
<img src="https://i.imgur.com/vtlyk2Z.png" height="28">

```
<vmq-checkbox
    v-model="checkbox"
    label="Label"
/>
```

#### Multiple
<img src="https://i.imgur.com/AZrdjX3.png" height="33">

```
<vmq-checkbox
    v-for="({ id, title }) in fruits"
    v-model="input"
    :label="title"
    :vmq-value="id"
    :key="id"
/>
data() {
    return {
        fruits: [
            { id: 1, title: "Bannan" },
            { id: 2, title: "Orange" },
            { id: 3, title: "Apple" },
        ],
        input: [1, 2]
    };
}
```

------

## API
#### Props

| Name                 | Type                | Description    | default    |
| :--------------------|:-------------------:|----------------|:-----------|
| `v-model`            | `bind`              |                |            |
| `labele`             | `string`            |
| `disabled`           | `boolean`           |                | `false`    |
| `containerClass`     | `string`

#### Event

| Name                 | Type                | Description    | default    |
| :--------------------|:-------------------:|----------------|:-----------|
| `change(value, event)` | `function`        | `Invoked when input changes`

## Style
#### Custom Style
Custom style with css variable

<img src="https://i.imgur.com/TFR4DFh.png" height="100">

```
<vmq-checkbox
    v-model="checkbox"
    label="Label"
/>

// for example to set as a global
<style>
    :root {
        --vmq-checkbox-color: tan;
        --vmq-checkbox-label-color: black;
    }
</style>
```